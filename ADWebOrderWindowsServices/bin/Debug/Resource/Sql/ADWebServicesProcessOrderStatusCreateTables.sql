USE [G733_DATA]

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessOrderStatus'))
	BEGIN
		SET ANSI_NULLS ON
		SET QUOTED_IDENTIFIER ON
		CREATE TABLE [imp].[ADWebWindowsServicesProcessOrderStatus](
			[CustomerId] [nvarchar](30) NOT NULL,
			[IdWebOrder] [nvarchar](30) NOT NULL,
			[IdADOrder] [int] NOT NULL,
			[IdADDelivaryNote] [int] NULL,
			[ADOrderDate] [datetime]  NULL,
			[UpdatedAt] [datetime]  NULL
		)
	END


IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessOrderStatusLog'))
	BEGIN
		SET ANSI_NULLS ON
		SET QUOTED_IDENTIFIER ON
		CREATE TABLE [imp].[ADWebWindowsServicesProcessOrderStatusLog](
			[IdWebOrder] [nvarchar](30) NOT NULL,
			[IdADOrder] [int] NOT NULL,
			[IdADDelivaryNote] [int] NULL,
			[TypeLog] [int] NOT NULL,
			[Message] [nvarchar](max) NOT NULL,
			[ExceptionJson] [nvarchar](max) NOT NULL,
			[CreatedAt] [datetime]  NOT NULL
		)
	END
