USE [G733_DATA]

DECLARE @Exist bit;
SET @Exist = (SELECT COUNT(*) FROM [imp].[ADWebWindowsServices] WITH (NOLOCK) WHERE [Name] = N'ADWebWindowsServicesOrder');
IF @Exist = 0 INSERT [imp].[ADWebWindowsServices] ([Name],[InstalledDate],[IntervalExecutionTimeMiliSecond]) VALUES (N'ADWebWindowsServicesOrder', convert([nvarchar](30), getdate(), 20), 3600000);
