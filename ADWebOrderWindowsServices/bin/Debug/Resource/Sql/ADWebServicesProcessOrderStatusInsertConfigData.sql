USE [G733_DATA]

DECLARE @IdServices [int];
DECLARE @Exist bit;

SET @IdServices = (SELECT [IdServices] FROM [imp].[ADWebWindowsServices] WHERE [Name] = N'ADWebWindowsServicesOrder');

SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcess] WITH (NOLOCK) 
		WHERE [IdServices] = @IdServices AND [Name] = N'ADWebWindowsServicesProcessOrderStatus');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcess] ([IdServices],[Name],[IsProcessRunning],[ScheduleTime]) 
	VALUES (@IdServices, N'ADWebWindowsServicesProcessOrderStatus', 0,N'00:00,2:00,8:00,10:00,12:00,14:00,16:00,18:00,20:00,22:00');
