USE [G733_DATA]

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessOrderStatus'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessOrderStatus]


IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessOrderStatusLog'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessOrderStatusLog]
