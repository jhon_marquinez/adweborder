



select  clis.xcliente_id CustomerId, g733_numpedweb IdWebOrder, albaranesweb.numped_id IdADOrder, ex.corefcli IdADDelivaryNote, ac.xfecha_pedido ADOrderDate, null UpdateAt
from imp.G733_dhl_exped ex left join imp.g733_dhl_palets pal
on ex.xpalet_num=pal.xpalet_num
and ex.xfecha_palet=pal.xfecha_palet
and year(ex.xfecha_palet)>2017
and year(pal.xfecha_palet)>2017
left join imp.g733_dhl_ficheros fich
on pal.xfichero_salida=fich.xfichero_salida
right join (
            select distinct hl.xnumdoc_id, popc.xnumdoc_id numped_id, g733_numpedweb
            from imp.pl_halbcli_lin hl right join imp.pl_hpedcli_opc popc
            on hl.xnumped_id=popc.xnumdoc_id
            and hl.xempresa_id=popc.xempresa_id
            
            and hl.xcicloped_id=popc.xciclo_id
            and hl.xtdocped_id=popc.xtipodoc_id
            and hl.xtipodoc_id=22
            and hl.xseccion_id=0
            and hl.xciclo_id>2017
            and hl.xempresa_id='al' 
            
            where  g733_origen=2
            and popc.xempresa_id='al'
            and popc.xseccion_id=0
            and popc.xtipodoc_id=21
            and popc.xciclo_id>2017
            and g733_numpedweb<>''
            
) albaranesweb
on ex.corefcli=albaranesweb.xnumdoc_id inner join
imp.pl_hpedcli_cab ac on  ac.xnumdoc_id=albaranesweb.numped_id and ac.xciclo_id>2017
left join imp.pc_clientes clis
on ac.xcliente_id=clis.xcliente_id
and ac.xempresa_id=clis.xempgen_id
where clis.xempgen_id='al'
and corefcli is not null
-- and not exists ( select xcorefcli from imp.g733_pedidos_web where xcorefcli=ex.corefcli and year(xfecha_pedido)>2017 )
and not exists ( select IdADDelivaryNote from imp.ADWebWindowsServicesProcessOrderStatus where IdADDelivaryNote=ex.corefcli and year(xfecha_pedido)>2017 )
and ex.corefcli not in ( select IdADDelivaryNote from imp.ADWebWindowsServicesProcessOrderStatusLog where  TypeLog = 1 and ExceptionJson like '%not exists in the Magento%' )

UNION

-- PEDIDOS QUE SALEN POR FURGONETA
select g733_cliente_id CustomerId, g733_numpedweb IdWebOrder, albaranesweb.numped_id IdADOrder, sf.g733_numdoc_id IdADDelivaryNote, ac.xfecha_pedido ADOrderDate, null UpdateAt
from imp.g733_salid_furgo sf inner join (
            select distinct hl.xnumdoc_id, popc.xnumdoc_id numped_id, g733_numpedweb
            from imp.pl_halbcli_lin hl right join imp.pl_hpedcli_opc popc
            on hl.xnumped_id=popc.xnumdoc_id
            and hl.xempresa_id=popc.xempresa_id
            
            and hl.xcicloped_id=popc.xciclo_id
            and hl.xtdocped_id=popc.xtipodoc_id
            and hl.xtipodoc_id=22
            and hl.xseccion_id=0
            and hl.xciclo_id>2017
            and hl.xempresa_id='al' 
            
            where  g733_origen=2
            and popc.xempresa_id='al'
            and popc.xseccion_id=0
            and popc.xtipodoc_id=21
            and popc.xciclo_id>2017
            and g733_numpedweb<>''
            
) albaranesweb
on sf.g733_numdoc_id = albaranesweb.xnumdoc_id
inner join
imp.pl_hpedcli_cab ac on  ac.xnumdoc_id=albaranesweb.numped_id and ac.xciclo_id>2017
where sf.g733_numdoc_id not in ( select IdADDelivaryNote from imp.ADWebWindowsServicesProcessOrderStatusLog where  TypeLog = 1 and ExceptionJson like '%not exists in the Magento%' )


order by 4 desc, 3 desc, 2 desc;

