﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ADWebWindowsServicesOrder
{
    static class Program
    {
        static void Main()
        {
#if DEBUG
            WebOrderWindowsServices service = new WebOrderWindowsServices();
            service.OnDebug();
            System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new WebOrderWindowsServices()
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
