﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ADWebWindowsServicesOrder
{
    [RunInstaller(true)]
    public partial class ADWebWindowsServicesOrderInstaller : System.Configuration.Install.Installer
    {
        public ADWebWindowsServicesOrderInstaller()
        {
            InitializeComponent();
        }

        private void WebOrderWindowsServiceInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            new ServiceController(WebWindowsServiceOrderInstaller.ServiceName).Start();
        }

        private void WebWindowsServiceOrderInstaller_BeforeInstall(object sender, InstallEventArgs e)
        {
            new ADWebWindowsServices.Manager.ADWebWindowsServicesManager().CheckConnetivityWithDatabaseEngine().CreateGeneralServicesTables();
            new ADWebServicesOrder.Manager.ADWebServicesOrderManager().CreateTables().InsertServicesConfigurationData().InsertProcessConfigurationData().InsertTaskConfigurationData();
        }

        private void WebWindowsServiceOrderInstaller_AfterUninstall(object sender, InstallEventArgs e)
        {
            ADWebWindowsServices.Manager.ADWebWindowsServicesManager ServicesManager = new ADWebWindowsServices.Manager.ADWebWindowsServicesManager();
            ADWebServicesOrder.Manager.ADWebServicesOrderManager ServiceOrderManager = new ADWebServicesOrder.Manager.ADWebServicesOrderManager();
            ServicesManager.CheckConnetivityWithDatabaseEngine();
            ServiceOrderManager.DeleteConfigurationData().DropTables();
            if (!ServicesManager.ThereIsSomeServicesInstalled())
                ServicesManager.DropGeneralServicesTables();
        }
    }
}
