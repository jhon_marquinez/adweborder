﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using ADWebWindowsServices.Entity;
using ADWebWindowsServices.Enum;
using ADTool.Log;
using ADTool.Serializer;
using ADWebWindowsServices.Manager;
using ADWebServicesOrder.Manager;

namespace ADWebWindowsServicesOrder
{
    partial class WebOrderWindowsServices : ServiceBase
    {
        private System.Timers.Timer timer = new System.Timers.Timer();
        private ADWebWindowsServicesManager ServicesConfiguration = new ADWebWindowsServicesManager();

        public WebOrderWindowsServices()
        {
            InitializeComponent();
            
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            ADWebWindowsServicesEntity Services = null;

            try
            {
                Services = ServicesConfiguration.GetServices(new ADWebServicesOrderManager().GetServicesName());
                timer = new System.Timers.Timer
                {
                    Interval = Services.IntervalExecutionTimeMiliSecond
                };

                timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
                timer.Start();
            }
            catch (Exception ex)
            {
                ex = ex.InnerException ?? ex;
                ADWebWindowsServicesException ADException = 
                    new ADWebWindowsServicesException(
                        ex.TargetSite.ToString(), 
                        ex.Message, 
                        ex.Source, 
                        ex.TargetSite.DeclaringType.ToString(), 
                        ex.TargetSite.MemberType.ToString(), 
                        ex.StackTrace
                    );

                try { ServicesConfiguration.AddLog(Services, ADWebWindowsServicesStatus.Stopped, LogType.Error, JsonSerializer.Serialize(ADException)); }
                catch (Exception excep)
                {
                    //TODO: manage exception, write error in file
                }
                //TODO: Send mail reporting an error as well
            }

            try { ServicesConfiguration.AddLog(Services, ADWebWindowsServicesStatus.Started); }
            catch (Exception ex)
            {
                //TODO: manage exception, write error in file
            }
        }

        protected override void OnStop()
        {
            ServicesConfiguration.AddLog(ServicesConfiguration.GetServices(new ADWebServicesOrderManager().GetServicesName()), ADWebWindowsServicesStatus.Stopped);
            new ADWebServicesOrderManager().StopAllProcessRunning();
            //TODO: Send mail reporting services status as well
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            ADWebServicesOrderManager OrderServices = null;
            ADWebWindowsServicesEntity Services = null;
            try
            {
                OrderServices = new ADWebServicesOrderManager();
                Services = ServicesConfiguration.GetServices(OrderServices.GetServicesName());
                timer.Interval = Services.IntervalExecutionTimeMiliSecond;
                timer.Stop();
                OrderServices.Initialize();
                timer.Start();
            }
            catch (Exception ex)
            {
                ex = ex.InnerException ?? ex;
                ADWebWindowsServicesException ADException =
                    new ADWebWindowsServicesException(
                        ex.TargetSite.ToString(),
                        ex.Message,
                        ex.Source,
                        ex.TargetSite.DeclaringType.ToString(),
                        ex.TargetSite.MemberType.ToString(),
                        ex.StackTrace
                    );
                try { ServicesConfiguration.AddLog(Services, ADWebWindowsServicesStatus.Started, LogType.Error, JsonSerializer.Serialize(ADException)); }
                catch (Exception excep)
                {
                    //TODO: manage exception, write error in file
                }
                timer.Start();
            }
        }
    }
}
