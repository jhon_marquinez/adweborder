﻿namespace ADWebWindowsServicesOrder
{
    partial class ADWebWindowsServicesOrderInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ADWebWindowsServicesOrderProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.WebWindowsServiceOrderInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ADWebWindowsServicesOrderProcessInstaller
            // 
            this.ADWebWindowsServicesOrderProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ADWebWindowsServicesOrderProcessInstaller.Password = null;
            this.ADWebWindowsServicesOrderProcessInstaller.Username = null;
            // 
            // WebWindowsServiceOrderInstaller
            // 
            this.WebWindowsServiceOrderInstaller.Description = "Service to update the order status in Alfa Dyser S,L\' web.";
            this.WebWindowsServiceOrderInstaller.DisplayName = "Alfa Dyser web order services";
            this.WebWindowsServiceOrderInstaller.ServiceName = "ADWebOrderServices";
            this.WebWindowsServiceOrderInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.WebWindowsServiceOrderInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.WebOrderWindowsServiceInstaller_AfterInstall);
            this.WebWindowsServiceOrderInstaller.AfterUninstall += new System.Configuration.Install.InstallEventHandler(this.WebWindowsServiceOrderInstaller_AfterUninstall);
            this.WebWindowsServiceOrderInstaller.BeforeInstall += new System.Configuration.Install.InstallEventHandler(this.WebWindowsServiceOrderInstaller_BeforeInstall);
            // 
            // ADWebWindowsServicesOrderInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ADWebWindowsServicesOrderProcessInstaller,
            this.WebWindowsServiceOrderInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ADWebWindowsServicesOrderProcessInstaller;
        private System.ServiceProcess.ServiceInstaller WebWindowsServiceOrderInstaller;
    }
}