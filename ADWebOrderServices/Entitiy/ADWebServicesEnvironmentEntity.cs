﻿using ADTool.Environment;
using ADTool.Data;
using System.Collections.Generic;

namespace ADWebServicesOrder.Entity
{
    internal class ADWebServicesEnvironmentEntity : IEnvironment
    {
        public string EnvironmentName { get; set; }
        public bool ActiveEnvironment { get; set; }
        public DataEnvironmentTypeEnum DataEnvironmentType { get; set; }
        public IEnumerable<DataConnection> Connections { get; set; }
        public string ServicesProcessOrderStatusTable { get; set; }
        public string ServicesProcessOrderStatusLogTable { get; set; }
        public string ServicesName { get; set; }
        public string ADWebServicesProcessOrderStatusTaskUpdateSelectOrderSQL { get; set; }
        public string ADWebServicesProcessOrderStatusCreateTables { get; set; }
        public string ADWebServicesProcessOrderStatusDropTables { get; set; }
        public string ADWebServicesOrderInsertConfigData { get; set; }
        public string ADWebServicesOrderDeleteConfigData { get; set; }
        public string ADWebServicesProcessOrderStatusInsertConfigData { get; set; }
        public string ADWebServicesProcessTaskOrderStatusInsertConfigData { get; set; }
    }
}
