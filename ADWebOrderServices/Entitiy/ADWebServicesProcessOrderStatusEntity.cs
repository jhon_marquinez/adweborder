﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADWebServicesOrder.Entity
{
    internal class ADWebServicesProcessOrderStatusEntity
    {
        public string CustomerId { get; set; }
        public string IdWebOrder { get; set; }
        public int IdADOrder { get; set; }
        public int IdADDelivaryNote { get; set; }
        public DateTime? ADOrderDate { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public ADWebServicesProcessOrderStatusEntity()
        {
        }

        public ADWebServicesProcessOrderStatusEntity(string customerId, string idWebOrder, int idADOrder, int idADDelivaryNote, DateTime? aDOrderDate, DateTime? updatedAt)
        {
            CustomerId = customerId;
            IdWebOrder = idWebOrder;
            IdADOrder = idADOrder;
            IdADDelivaryNote = idADDelivaryNote;
            ADOrderDate = aDOrderDate;
            UpdatedAt = updatedAt;
        }
    }
}
