﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADTool.Log;

namespace ADWebServicesOrder.Entity
{
    class ADWebServicesProcessOrderStatusLogEntity
    {
        public string IdWebOrder { get; set; }
        public int IdADOrder { get; set; }
        public int IdADDelivaryNote { get; set; }
        public ADTool.Log.LogType TypeLog { get; set; }
        public string Message { get; set; }
        public string ExceptionJson { get; set; }
        public DateTime? CreatedAt { get; set; }

        public ADWebServicesProcessOrderStatusLogEntity()
        {
        }

        public ADWebServicesProcessOrderStatusLogEntity(string idWebOrder, int idADOrder, int idADDelivaryNote, LogType typeLog, string message, string exceptionJson, DateTime? createdAt)
        {
            IdWebOrder = idWebOrder;
            IdADOrder = idADOrder;
            IdADDelivaryNote = idADDelivaryNote;
            TypeLog = typeLog;
            Message = message;
            ExceptionJson = exceptionJson;
            CreatedAt = createdAt;
        }
    }
}
