﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebServicesOrder.Repository;
using ADWebServicesOrder.Resource;
using ADWebServicesOrder.Process;
using ADTool.Log;
using ADTool.Serializer;
using ADWebWindowsServices.Manager;
using ADWebWindowsServices.Entity;
using ADWebWindowsServices.Enum;

namespace ADWebServicesOrder.Manager
{
    public class ADWebServicesOrderManager
    {
        /// <summary>
        /// 
        /// </summary>
        private ADWebWindowsServicesManager ServicesConfiguration;

        /// <summary>
        /// 
        /// </summary>
        public ADWebServicesOrderManager()
        {
            ServicesConfiguration = new ADWebWindowsServicesManager();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public string GetServicesName()
        {
            using (ADWebServicesOrderRepository OrderRepository = new ADWebServicesOrderRepository())
            {
                return OrderRepository.GetServicesName();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            ADWebWindowsServicesEntity Services = ServicesConfiguration.GetServices(GetServicesName());
            IEnumerable<ADWebWindowsServicesProcessEntity> ProcessRunning = ServicesConfiguration.GetProcessRunning(Services);
            if(ProcessRunning.Count() > 0)
                ServicesConfiguration.AddLogProcessRunning(ProcessRunning);

            IEnumerable<ADWebWindowsServicesProcessEntity> ProcessNotRunning = ServicesConfiguration.GetProcessNotRunning(Services);
            foreach (ADWebWindowsServicesProcessEntity Process in ProcessNotRunning)
            {
                if (!ServicesConfiguration.IsAllowExecutionThisProcessAfterCheckScheduleTime(Process)) break;

                ServicesConfiguration.StartProcess(Process);
                ServicesConfiguration.AddLog(Process, $"The process {Process.Name} has been started.");

                try
                {
                    ExecuteServicesProcess(Process);
                    ServicesConfiguration.AddLog(Process, $"The process {Process.Name} has been execute correctly.");
                }
                catch (Exception ex)
                {
                    ex = ex.InnerException ?? ex;
                    ADWebWindowsServicesException ADException =
                        new ADWebWindowsServicesException(
                            ex.TargetSite.ToString(),
                            ex.Message,
                            ex.Source,
                            ex.TargetSite.DeclaringType.ToString(),
                            ex.TargetSite.MemberType.ToString(),
                            ex.StackTrace
                        );
                    try { ServicesConfiguration.AddLog(Process, $"Error while the process {Process.Name} was executing.", LogType.Error, JsonSerializer.Serialize(ADException)); }
                    catch (Exception excep)
                    {
                        //TODO: Añadir log en fichero
                    }
                }

                ServicesConfiguration.StopProcess(Process);
                ServicesConfiguration.AddLog(Process, $"The process {Process.Name} has been stopped.");
            }
            
        }
        /// <summary>
        /// 
        /// </summary>
        public void StopAllProcessRunning()
        {
            ADWebWindowsServicesEntity Services = ServicesConfiguration.GetServices(GetServicesName());
            IEnumerable<ADWebWindowsServicesProcessEntity> ProcessRunning = ServicesConfiguration.GetProcessRunning(Services);
            if (ProcessRunning.Count() > 0)
                foreach (ADWebWindowsServicesProcessEntity Process in ProcessRunning)
                {
                    ServicesConfiguration.StopProcess(Process);
                    ServicesConfiguration.AddLog(Process, $"The process {Process.Name} has been stopped.");
                }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <exception cref="Exception"></exception>
        internal void ExecuteServicesProcess(ADWebWindowsServicesProcessEntity Process)
        {
            IEnumerable<ADWebWindowsServicesProcessTaskEntity> ActiveTask = ServicesConfiguration.GetTaskActive(Process);

            foreach (ADWebWindowsServicesProcessTaskEntity Task in ActiveTask)
            {
                try
                {
                    ServicesConfiguration.StartTask(Task);
                    ServicesConfiguration.AddLog(Task, $"The task {Task.Name} has been started.");

                    if (Process.Name == ConstantValue.ServicesProcessOrderStatus)
                    {
                        new ADWebServicesProcessOrderStatus().RunTask(Task);
                        ServicesConfiguration.AddLog(Task, $"The task {Task.Name} has been execute correctly.");
                    }
                }
                catch (Exception ex)
                {
                    ex = ex.InnerException ?? ex;
                    ADWebWindowsServicesException ADException =
                        new ADWebWindowsServicesException(
                            ex.TargetSite.ToString(),
                            ex.Message,
                            ex.Source,
                            ex.TargetSite.DeclaringType.ToString(),
                            ex.TargetSite.MemberType.ToString(),
                            ex.StackTrace
                        );
                    try { ServicesConfiguration.AddLog(Task, $"Error while the task {Process.Name} was executing.", LogType.Error, JsonSerializer.Serialize(ADException)); }
                    catch (Exception excep)
                    {
                        //TODO: Añadir log en fichero
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ADWebServicesOrderManager CreateTables()
        {
            using (ADWebServicesOrderRepository OrderRepository = new ADWebServicesOrderRepository())
            {
                OrderRepository.CreateTables();
            }
            return this;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ADWebServicesOrderManager InsertServicesConfigurationData()
        {
            using (ADWebServicesOrderRepository OrderRepository = new ADWebServicesOrderRepository())
            {
                OrderRepository.InsertServicesConfigurationData();
            }
            return this;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ADWebServicesOrderManager InsertProcessConfigurationData()
        {
            using (ADWebServicesOrderRepository OrderRepository = new ADWebServicesOrderRepository())
            {
                OrderRepository.InsertProcessConfigurationData();
            }
            return this;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ADWebServicesOrderManager InsertTaskConfigurationData()
        {
            using (ADWebServicesOrderRepository OrderRepository = new ADWebServicesOrderRepository())
            {
                OrderRepository.InsertTaskConfigurationData();
            }
            return this;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ADWebServicesOrderManager DeleteConfigurationData()
        {
            using (ADWebServicesOrderRepository OrderRepository = new ADWebServicesOrderRepository())
            {
                OrderRepository.DeleteServicesConfigurationData();
            }
            return this;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ADWebServicesOrderManager DropTables()
        {
            using (ADWebServicesOrderRepository OrderRepository = new ADWebServicesOrderRepository())
            {
                OrderRepository.DropTables();
            }
            return this;
        }
    }
}
