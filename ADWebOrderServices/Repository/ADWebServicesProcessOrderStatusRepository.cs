﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ADTool.Data.DapperClient;
using ADWebServicesOrder.Query;
using ADWebServicesOrder.Entity;

namespace ADWebServicesOrder.Repository
{
    internal class ADWebServicesProcessOrderStatusRepository: IDisposable
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// 
        /// </summary>
        private bool disposed = false;
        /// <summary>
        /// 
        /// </summary>
        private ADWebServicesProcessOrderStatusQuery OrderStatusQuery = null;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public ADWebServicesProcessOrderStatusRepository()
        {
            OrderStatusQuery = new ADWebServicesProcessOrderStatusQuery();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ADWebServicesProcessOrderStatusEntity> GetOrdersToUpdateStatus()
        {
            return OrderStatusQuery.GetOrdersToUpdateStatus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="OrdertStatusEntity"></param>
        public void SetStatusAsSentToOrder(ADWebServicesProcessOrderStatusEntity OrdertStatusEntity)
        {
            OrderStatusQuery.SetStatusAsSentToOrder(OrdertStatusEntity);
            OrderStatusQuery.SaveOrderStatusEntity(OrdertStatusEntity);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="OrderStatusEntity"></param>
        /// <param name="message"></param>
        /// <param name="logType"></param>
        /// <param name="exceptionJson"></param>
        public void AddLog(ADWebServicesProcessOrderStatusEntity OrderStatusEntity, string message, ADTool.Log.LogType logType, string exceptionJson = null)
        {
            OrderStatusQuery.AddLog(OrderStatusEntity, message, logType, exceptionJson);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="OrderStatusEntity"></param>
        /// <param name="message"></param>
        /// <param name="logType"></param>
        /// <param name="exceptionJson"></param>
        public void AddLog(IEnumerable<ADWebServicesProcessOrderStatusEntity> OrderStatusEntity, string message, ADTool.Log.LogType logType, string exceptionJson)
        {
            OrderStatusQuery.AddLog(OrderStatusEntity, message, logType, exceptionJson);

        }



        #region DISPOSE METHODS
        /// <summary>
        /// DatabaseController Destructor
        /// </summary>
        ~ADWebServicesProcessOrderStatusRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// DatabaseController Destructor
        /// </summary>
        /// <summary>
        /// Dispose the DDBB.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            /* Take yourself off the Finalization queue to prevent finalization code
             * for this object from executing a second time.
             */
            GC.SuppressFinalize(this);
        }

        /* Dispose(bool disposing) executes in two distinct scenarios.
         * If disposing equals true, the method has been called directly or indirectly
         * by a user's code. Managed and unmanaged resources can be disposed.
         * If disposing equals false, the method has been called by the runtime from
         * inside the finalizer and you should not reference other objects. Only
         * unmanaged resources can be disposed.
         */
        /// <summary>
        /// Virtual dispose the DDBB.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    OrderStatusQuery.Dispose();
                    OrderStatusQuery = null;
                }
                /* Release unmanaged resources. If disposing is false, only the following code
                 * is executed. Note that this is not thread safe. Another thread could start
                 * disposing the object after the managed resources are disposed, but before
                 * the disposed flag is set to true. If thread safety is necessary, it must be
                 * implemented by the client.
                 */
            }
            disposed = true;
        }
        #endregion

    }
}
