﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebWindowsServices.Entity;
using ADWebServicesOrder.Query;
using ADWebServicesOrder.Resource;

namespace ADWebServicesOrder.Repository
{
    internal class ADWebServicesOrderRepository : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        private bool disposed = false;
        /// <summary>
        /// 
        /// </summary>
        private ADWebServicesOrderQuery OrderQuery = null;

        /// <summary>
        /// 
        /// </summary>
        public ADWebServicesOrderRepository()
        {
            OrderQuery = new ADWebServicesOrderQuery();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public string GetServicesName()
        {
            return OrderQuery.GetServicesName();
        }
        /// <summary>
        /// 
        /// </summary>
        public void CreateTables()
        {
            OrderQuery.CreateTables();
        }
        /// <summary>
        /// 
        /// </summary>
        public void InsertServicesConfigurationData()
        {
            OrderQuery.InsertServicesConfigurationData();
        }
        /// <summary>
        /// 
        /// </summary>
        public void InsertProcessConfigurationData()
        {
            OrderQuery.InsertProcessConfigurationData();
        }
        /// <summary>
        /// 
        /// </summary>
        public void InsertTaskConfigurationData()
        {
            OrderQuery.InsertTaskConfigurationData();
        }
        /// <summary>
        /// 
        /// </summary>
        public void DeleteServicesConfigurationData()
        {
            OrderQuery.DeleteServicesConfigurationData();
        }
        /// <summary>
        /// 
        /// </summary>
        public void DropTables()
        {
            OrderQuery.DropTables();
        }

        #region DISPOSE METHODS
        /// <summary>
        /// ADWebServicesOrderConfigRepository Destructor
        /// </summary>
        ~ADWebServicesOrderRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// DatabaseController Destructor
        /// </summary>
        /// <summary>
        /// Dispose the DDBB.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            /* Take yourself off the Finalization queue to prevent finalization code
             * for this object from executing a second time.
             */
            GC.SuppressFinalize(this);
        }

        /* Dispose(bool disposing) executes in two distinct scenarios.
         * If disposing equals true, the method has been called directly or indirectly
         * by a user's code. Managed and unmanaged resources can be disposed.
         * If disposing equals false, the method has been called by the runtime from
         * inside the finalizer and you should not reference other objects. Only
         * unmanaged resources can be disposed.
         */
        /// <summary>
        /// Virtual dispose the DDBB.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    OrderQuery.Dispose();
                    OrderQuery = null;
                }
                /* Release unmanaged resources. If disposing is false, only the following code
                 * is executed. Note that this is not thread safe. Another thread could start
                 * disposing the object after the managed resources are disposed, but before
                 * the disposed flag is set to true. If thread safety is necessary, it must be
                 * implemented by the client.
                 */
            }
            disposed = true;
        }
        #endregion
    }
}
