﻿
namespace ADWebServicesOrder.Resource
{
    internal class ConstantValue
    {
        public static string ServicesProcessOrderStatus = "ADWebWindowsServicesProcessOrderStatus";
        public static string ServicesProcessOrderStatusTaskUpdate = "ADWebWindowsServicesProcessOrderStatusTaskUpdate";
        public static string FormatDateTime = "yyyy-MM-dd HH:mm:ss";
        public static string ValueForSetOrderLikeSent = "enviado";
        public static string WebTableOrder = "sales_flat_order";
    }
}
