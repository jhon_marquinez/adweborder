USE [G733_DATA]
GO

DECLARE @IdServices [int];
DECLARE @IdProcess [int];

SET @IdServices = (SELECT [IdServices] FROM [imp].[ADWebWindowsServices] WHERE [Name] = N'ADWebWindowsServicesOrder');

DECLARE @Exist bit;
SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcess] WITH (NOLOCK) 
		WHERE [IdServices] = @IdServices AND [Name] = N'ADWebWindowsServicesProcessOrderStatus');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcess] ([IdServices],[Name],[IsProcessRunning],[ScheduleTime]) 
	VALUES (@IdServices, N'ADWebWindowsServicesProcessOrderStatus', 0, N'7:00,09:00,12:45,22:30,00:50');

/*
SET @IdProcess = (SELECT [IdProcess] FROM [imp].[ADWebWindowsServicesProcess] WHERE [Name] = N'ADWebWindowsServicesProcessOrderStatus');


SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcessTask] WITH (NOLOCK) 
		WHERE [IdProcess] = @IdProcess AND [Name] = N'ADWebWindowsServicesProcessOrderStatusTaskUpdate');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcessTask] ([IdProcess], [Name], [ActiveTask]) 
		VALUES (@IdProcess, N'ADWebWindowsServicesProcessOrderStatusTaskUpdate', 1);

GO*/


/*
SELECT * FROM IMP.[ADWebWindowsServices]
SELECT * FROM IMP.[ADWebWindowsServicesProcess]
SELECT * FROM IMP.[ADWebWindowsServicesProcessTask]
SELECT * FROM IMP.[ADWebWindowsServicesLog]
SELECT * FROM IMP.[ADWebWindowsServicesProcessLog]
SELECT * FROM IMP.[ADWebWindowsServicesProcessTaskLog]
SELECT * FROM [imp].[ADWebWindowsServicesProcessExecutionLog]
SELECT * FROM IMP.[ADWebWindowsServicesProcessConfigData]
SELECT * FROM [imp].[ADWebWindowsServicesProcessTaskConfigData]
*/
/*
SELECT * FROM IMP.[ADWebWindowsServicesProcessLog]
SELECT * FROM IMP.[ADWebWindowsServicesProcessTaskLog]
SELECT * FROM [imp].[ADWebWindowsServicesProcessExecutionLog]
select * from [imp].[ADWebWindowsServicesProcessOrderStatus] -- where IdWebOrder = '110003899';
select * from [imp].[ADWebWindowsServicesProcessOrderStatusLog] order by CreatedAt desc;

INSERT INTO [imp].[ADWebWindowsServicesLog]([IdLog],[IdServices],[Status],[ExceptionsJson],[TypeLog],[CreatedAt]) VALUES(@IdLog, @IdEntity, @Status, @ExceptionJson, @TypeLog, @CreateAt);

	
delete FROM IMP.[ADWebWindowsServicesLog]
delete FROM IMP.[ADWebWindowsServicesProcessLog]
delete FROM IMP.[ADWebWindowsServicesProcessTaskLog]
delete from IMP.ADWebWindowsServicesProcessExecutionLog
delete from IMP.[ADWebWindowsServicesProcessOrderStatus]
delete from IMP.[ADWebWindowsServicesProcessOrderStatusLog]

SELECT [IdLog], [CreatedAt], [IdProcessRunning], [IdProcessSuccessExecution], [IdProcessFailedExecution], [IdTasksSuccessExecution], [IdTasksFailedExecution], [ExceptionsJsonList], [ItIsProcessing] FROM IMP.[ADWebWindowsServicesLog]

SELECT [IdExecutionLog], [IdProcess], [ScheduleTime], [Date] FROM [imp].[ADWebWindowsServicesProcessExecutionLog] WHERE [IdProcess] = 1;

select * from imp.ADWebWindowsServicesProcessOrderStatus where IdWebOrder = '110003924'

SELECT * FROM IMP.[ADWebWindowsServicesProccess]
UPDATE [imp].[ADWebWindowsServicesProcess] SET IsProcessRunning=0 WHERE [IdServices] = 1 AND [IdProcess] = 1;
UPDATE [imp].[ADWebWindowsServicesProcess] SET InitialStart=null WHERE [IdServices] = 1 AND [IdProcess] = 1;


SELECT * FROM [imp].[ADWebWindowsServicesProccess] WHERE [Name] = 'ADWebWindowsServicesOrder';

SELECT * FROM IMP.[ADWebWindowsServicesProccessTask];

update IMP.[ADWebWindowsServicesProccessTask] set ActiveTask = 1 where IdProcess = 1




*/