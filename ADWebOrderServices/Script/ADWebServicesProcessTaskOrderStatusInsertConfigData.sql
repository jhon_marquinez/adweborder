USE [G733_DATA]
GO

SET @IdProcess = (SELECT [IdProcess] FROM [imp].[ADWebWindowsServicesProcess] WHERE [Name] = N'ADWebWindowsServicesProcessOrderStatus');


SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcessTask] WITH (NOLOCK) 
		WHERE [IdProcess] = @IdProcess AND [Name] = N'ADWebWindowsServicesProcessOrderStatusTaskUpdate');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcessTask] ([IdProcess], [Name], [ActiveTask]) 
		VALUES (@IdProcess, N'ADWebWindowsServicesProcessOrderStatusTaskUpdate', 1);

GO