USE [G733_DATA]
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessOrderStatus'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessOrderStatus]
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessOrderStatusLog'))
DROP TABLE [imp].[ADWebWindowsServicesProcessOrderStatusLog]
GO