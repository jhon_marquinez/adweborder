﻿using ADTool.Environment;
using ADTool.CustomFile;
using ADTool.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebServicesOrder.Entity;
using System.IO;


namespace ADWebServicesOrder.Query
{
    internal class ADWebServicesOrderEnvironmentQuery
    {
        /// <summary>
        /// Private environment manager.
        /// </summary>
        private EnvironmentManager<ADWebServicesEnvironmentEntity> envManager = null;
        /// <summary>
        /// Environment file name.
        /// </summary>
        private const string envConfigFileName = "ConfigFile\\ADWebWindowsServicesOrderEnv.cfg";

        /// <summary>
        /// Constructor.
        /// </summary>
        public ADWebServicesOrderEnvironmentQuery()
        {
            envManager = new EnvironmentManager<ADWebServicesEnvironmentEntity>();
        }

        /// <summary>
        /// Load the environment if it's not loaded yet.
        /// </summary>
        /// <exception cref="Exception"></exception>
        private void LoadEnvironmentData()
        {
            if (!envManager.IsEnvironmentLoaded())
            {
                string pathEnvConfig = Path.Combine(FileManager.GetApplicationPath(), envConfigFileName);
                envManager.LoadEnvironment(pathEnvConfig);
            }
        }
        /// <summary>
        /// Get the connection string.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public string GetConnectionString(string connectionName, DataConnectionTypeProviderEnum typeProviderEnum)
        {
            LoadEnvironmentData();
            return envManager.ActivEnvironment().Connections.Where(conn => conn.ConnectionName == connectionName).First().GetConnectionString(typeProviderEnum);
        }
        /// <summary>
        /// Get the connection object.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public DataConnection GetConnection(string connectionName)
        {
            LoadEnvironmentData();
            return envManager.ActivEnvironment().Connections.Where(conn => conn.ConnectionName == connectionName).First();
        }
        /// <summary>
        /// Get the active environment.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public ADWebServicesEnvironmentEntity GetActivEnvironment()
        {
            LoadEnvironmentData();
            return envManager.ActivEnvironment();
        }
    }
}
