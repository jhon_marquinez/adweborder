﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ADTool.Data.DapperClient;
using ADTool.Data;
using ADTool.CustomFile;
using ADTool.Environment;
using ADWebServicesOrder.Entity;
using ADWebServicesOrder.Resource;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using ADWebWindowsServices.Resource;

namespace ADWebServicesOrder.Query
{
    internal class ADWebServicesProcessOrderStatusQuery: IDisposable
    {
        #region - P R O P E R T I E S
        private bool disposed = false;
        /// <summary>
        /// 
        /// </summary>
        private DapperModel DBModel = null;
        /// <summary>
        /// 
        /// </summary>
        private DapperManager DBManager = null;
        /// <summary>
        /// 
        /// </summary>
        private string connectionString = String.Empty;
        /// <summary>
        /// 
        /// </summary>
        private string connectionStringWeb = String.Empty;
        /// <summary>
        /// 
        /// </summary>
        private ADWebServicesOrderEnvironmentQuery EnvironmentQuery = null;
        #endregion

        #region - C O N S T R U C T O R S
        public ADWebServicesProcessOrderStatusQuery()
        {
        }
        #endregion

        #region - M E T H O D S

        /// <summary>
        /// 
        /// </summary>
        private void InitializeInstances()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesOrderEnvironmentQuery();
            if (String.IsNullOrEmpty(connectionString)) connectionString = EnvironmentQuery.GetConnectionString(ADWebWindowsServicesConstantValue.DBConnectionNameAlfaDyser, DataConnectionTypeProviderEnum.MSSQL);
            if (String.IsNullOrEmpty(connectionStringWeb)) connectionStringWeb = EnvironmentQuery.GetConnectionString(ADWebWindowsServicesConstantValue.DBConnectionNameAlfaDyserWeb, DataConnectionTypeProviderEnum.MYSQL);
            if (DBManager == null) DBManager = new DapperManager();
            if (DBModel == null) DBModel = new DapperModel();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool AreInstancesLoaded()
        {
            return (EnvironmentQuery != null) && (!String.IsNullOrEmpty(connectionString)) && (DBManager != null) && (DBModel != null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ADWebServicesProcessOrderStatusEntity> GetOrdersToUpdateStatus()
        {
            IEnumerable<ADWebServicesProcessOrderStatusEntity> result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = FileManager.GetFileFromAppPath(EnvironmentQuery.GetActivEnvironment().ADWebServicesProcessOrderStatusTaskUpdateSelectOrderSQL);
            DBModel.Connection = new SqlConnection { ConnectionString = DBModel.ConnectionString = connectionString };
            result = DBManager.Query<ADWebServicesProcessOrderStatusEntity>(DBModel, query);
            return result;
        }

        public void SetStatusAsSentToOrder(ADWebServicesProcessOrderStatusEntity OrderStatusEntity)
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"UPDATE {ConstantValue.WebTableOrder} SET status = '{ConstantValue.ValueForSetOrderLikeSent}' 
                                WHERE increment_id = @idWebOrder;";
            DBModel.Connection = new MySqlConnection { ConnectionString = DBModel.ConnectionString = connectionStringWeb };

            string currentStatusORder = DBManager.QueryFirstOrDefault<string>(DBModel, $"SELECT status FROM {ConstantValue.WebTableOrder} WHERE increment_id = {OrderStatusEntity.IdWebOrder};");

            if (String.IsNullOrEmpty(currentStatusORder))
            {
                throw new Exception(
                        String.Format("It wasn't updated the status of web order {0} because this order not exists in the Magento's database.",
                                        OrderStatusEntity.IdWebOrder, OrderStatusEntity.CustomerId, OrderStatusEntity.IdADOrder));
            }

            if (currentStatusORder != ConstantValue.ValueForSetOrderLikeSent)
            {
                bool someOrderAffected = Convert.ToBoolean(DBManager.Update(DBModel, query, new { idWebOrder = OrderStatusEntity.IdWebOrder }));
                if (!someOrderAffected)
                {
                    throw new Exception(
                        String.Format("It has not been updated the status of web order {0} to sent, of client {1}, with web order internal code {2}.",
                                        OrderStatusEntity.IdWebOrder, OrderStatusEntity.CustomerId, OrderStatusEntity.IdADOrder));
                }
                OrderStatusEntity.UpdatedAt = DateTime.Now;
            }
        }

        public void SaveOrderStatusEntity(ADWebServicesProcessOrderStatusEntity OrderStatusEntity)
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"INSERT INTO {EnvironmentQuery.GetActivEnvironment().ServicesProcessOrderStatusTable}([CustomerId],[IdWebOrder],[IdADOrder],[IdADDelivaryNote],[ADOrderDate],[UpdatedAt]) 
                            VALUES(@CustomerId, @IdWebOrder, @IdADOrder, @IdADDelivaryNote, @ADOrderDate, @UpdatedAt);";
            DBModel.Connection = new SqlConnection { ConnectionString = DBModel.ConnectionString = connectionString };
            OrderStatusEntity.UpdatedAt = OrderStatusEntity.UpdatedAt ?? DateTime.Now;
            DBManager.Insert(DBModel, query, OrderStatusEntity);
        }

        public void AddLog(ADWebServicesProcessOrderStatusEntity OrderStatusEntity, string message, ADTool.Log.LogType logType, string exceptionJson = null)
        {
            AddLog(new List<ADWebServicesProcessOrderStatusEntity> { OrderStatusEntity }.AsEnumerable(), message, logType, exceptionJson);
        }

        public void AddLog(IEnumerable<ADWebServicesProcessOrderStatusEntity> OrderStatusEntity, string message, ADTool.Log.LogType logType, string exceptionJson = null)
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"INSERT INTO {EnvironmentQuery.GetActivEnvironment().ServicesProcessOrderStatusLogTable} ([IdWebOrder],[IdADOrder],[IdADDelivaryNote],[TypeLog],[Message],[ExceptionJson],[CreatedAt]) 
                            VALUES(@IdWebOrder, @IdADOrder, @IdADDelivaryNote, @TypeLog, @Message, @ExceptionJson, @CreatedAt);";
            DBModel.Connection = new SqlConnection { ConnectionString = DBModel.ConnectionString = connectionString };
            foreach (ADWebServicesProcessOrderStatusEntity OrderStatus in OrderStatusEntity)
            {
                DBManager.Insert(
                    DBModel, 
                    query, 
                    new ADWebServicesProcessOrderStatusLogEntity(
                        OrderStatus.IdWebOrder, 
                        OrderStatus.IdADOrder, 
                        OrderStatus.IdADDelivaryNote,
                        logType, 
                        message, 
                        exceptionJson ?? String.Empty, 
                        DateTime.Now
                    )
                );
            }
        }


        #region DISPOSE METHODS
        /// <summary>
        /// ADWebServicesProcessOrderStatusQuery Destructor
        /// </summary>
        ~ADWebServicesProcessOrderStatusQuery()
        {
            Dispose(false);
        }

        /// <summary>
        /// DatabaseController Destructor
        /// </summary>
        /// <summary>
        /// Dispose the DDBB.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            /* Take yourself off the Finalization queue to prevent finalization code
             * for this object from executing a second time.
             */
            GC.SuppressFinalize(this);
        }

        /* Dispose(bool disposing) executes in two distinct scenarios.
         * If disposing equals true, the method has been called directly or indirectly
         * by a user's code. Managed and unmanaged resources can be disposed.
         * If disposing equals false, the method has been called by the runtime from
         * inside the finalizer and you should not reference other objects. Only
         * unmanaged resources can be disposed.
         */
        /// <summary>
        /// Virtual dispose the DDBB.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (DBManager != null) DBManager.Dispose();
                    if (DBModel != null) DBModel.Dispose();
                    DBManager = null;
                    DBModel = null;
                    EnvironmentQuery = null;
                    connectionString = String.Empty;
                }
                /* Release unmanaged resources. If disposing is false, only the following code
                 * is executed. Note that this is not thread safe. Another thread could start
                 * disposing the object after the managed resources are disposed, but before
                 * the disposed flag is set to true. If thread safety is necessary, it must be
                 * implemented by the client.
                 */
            }
            disposed = true;
        }
        #endregion

        #endregion
    }
}
