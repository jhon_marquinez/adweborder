﻿using System;
using System.Collections.Generic;
using System.Linq;
using ADWebServicesOrder.Entity;
using ADTool.Data.DapperClient;
using ADTool.Data;
using ADWebServicesOrder.Resource;
using ADWebWindowsServices.Resource;
using ADWebWindowsServices.Entity;

namespace ADWebServicesOrder.Query
{
    internal class ADWebServicesOrderQuery: IDisposable
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// property to know if has been disposed some this class's instance 
        /// </summary>
        private bool disposed = false;
        /// <summary>
        /// object to storage the object for access to database
        /// </summary>
        private DapperModel DBModel = null;
        /// <summary>
        /// object to manage the access to database
        /// </summary>
        private DapperManager DBManager = null;
        /// <summary>
        /// 
        /// </summary>
        private string connectionString = String.Empty;
        /// <summary>
        /// 
        /// </summary>
        private ADWebServicesOrderEnvironmentQuery EnvironmentQuery = null;
        #endregion

        #region - C O N S T R U C T O R S
        /// <summary>
        /// 
        /// </summary>
        public ADWebServicesOrderQuery()
        {
        }
        #endregion

        #region - M E T H  O D S

        /// <summary>
        /// 
        /// </summary>
        private void InitializeInstances()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesOrderEnvironmentQuery();
            if (String.IsNullOrEmpty(connectionString)) connectionString = EnvironmentQuery.GetConnectionString(ADWebWindowsServicesConstantValue.DBConnectionNameAlfaDyser, DataConnectionTypeProviderEnum.MSSQL);
            if (DBManager == null) DBManager = new DapperManager();
            if (DBModel == null) DBModel = new DapperModel(connectionString, DataConnectionTypeProviderEnum.MSSQL);
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool AreInstancesLoaded()
        {
            return (EnvironmentQuery != null) && (!String.IsNullOrEmpty(connectionString)) && (DBManager != null) && (DBModel != null);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public string GetServicesName()
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            return EnvironmentQuery.GetActivEnvironment().ServicesName;
        }
        /// <summary>
        /// 
        /// </summary>
        public void CreateTables()
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = ADTool.CustomFile.FileManager.GetFileFromAppPath(EnvironmentQuery.GetActivEnvironment().ADWebServicesProcessOrderStatusCreateTables);
            DBManager.Execute(DBModel, query);
        }
        /// <summary>
        /// 
        /// </summary>
        public void InsertServicesConfigurationData()
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = ADTool.CustomFile.FileManager.GetFileFromAppPath(EnvironmentQuery.GetActivEnvironment().ADWebServicesOrderInsertConfigData);
            DBManager.Execute(DBModel, query);
        }
        /// <summary>
        /// 
        /// </summary>
        public void InsertProcessConfigurationData()
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = ADTool.CustomFile.FileManager.GetFileFromAppPath(EnvironmentQuery.GetActivEnvironment().ADWebServicesProcessOrderStatusInsertConfigData);
            DBManager.Execute(DBModel, query);
        }
        /// <summary>
        /// 
        /// </summary>
        public void InsertTaskConfigurationData()
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = ADTool.CustomFile.FileManager.GetFileFromAppPath(EnvironmentQuery.GetActivEnvironment().ADWebServicesProcessTaskOrderStatusInsertConfigData);
            DBManager.Execute(DBModel, query);
        }
        /// <summary>
        /// 
        /// </summary>
        public void DeleteServicesConfigurationData()
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = ADTool.CustomFile.FileManager.GetFileFromAppPath(EnvironmentQuery.GetActivEnvironment().ADWebServicesOrderDeleteConfigData);
            DBManager.Execute(DBModel, query);
        }
        /// <summary>
        /// 
        /// </summary>
        public void DropTables()
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = ADTool.CustomFile.FileManager.GetFileFromAppPath(EnvironmentQuery.GetActivEnvironment().ADWebServicesProcessOrderStatusDropTables);
            DBManager.Execute(DBModel, query);
        }

        #region DISPOSE METHODS
        /// <summary>
        /// ADWebServicesOrderConfigQuery Destructor
        /// </summary>
        ~ADWebServicesOrderQuery()
        {
            Dispose(false);
        }

        /// <summary>
        /// DatabaseController Destructor
        /// </summary>
        /// <summary>
        /// Dispose the DDBB.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            /* Take yourself off the Finalization queue to prevent finalization code
             * for this object from executing a second time.
             */
            GC.SuppressFinalize(this);
        }

        /* Dispose(bool disposing) executes in two distinct scenarios.
         * If disposing equals true, the method has been called directly or indirectly
         * by a user's code. Managed and unmanaged resources can be disposed.
         * If disposing equals false, the method has been called by the runtime from
         * inside the finalizer and you should not reference other objects. Only
         * unmanaged resources can be disposed.
         */
        /// <summary>
        /// Virtual dispose the DDBB.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (DBManager != null) DBManager.Dispose();
                    if (DBModel != null) DBModel.Dispose();
                    DBManager = null;
                    DBModel = null;
                    EnvironmentQuery = null;
                    connectionString = String.Empty;
                }
                /* Release unmanaged resources. If disposing is false, only the following code
                 * is executed. Note that this is not thread safe. Another thread could start
                 * disposing the object after the managed resources are disposed, but before
                 * the disposed flag is set to true. If thread safety is necessary, it must be
                 * implemented by the client.
                 */
            }
            disposed = true;
        }
        #endregion

        #endregion

    }
}
