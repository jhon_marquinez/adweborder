﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebServicesOrder.Repository;
using ADWebServicesOrder.Entity;
using ADWebServicesOrder.Resource;
using ADWebWindowsServices.Entity;

namespace ADWebServicesOrder.Process
{
    internal class ADWebServicesProcessOrderStatus
    {
        /// <summary>
        /// 
        /// </summary>
        private ADWebServicesProcessOrderStatusRepository OrderStatusRepository;
        /// <summary>
        /// 
        /// </summary>
        public ADWebServicesProcessOrderStatus()
        {
            OrderStatusRepository = new ADWebServicesProcessOrderStatusRepository();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="TaskEntity"></param>
        public void RunTask(ADWebWindowsServicesProcessTaskEntity TaskEntity)
        {
            if (TaskEntity.Name == ConstantValue.ServicesProcessOrderStatusTaskUpdate)
            {
                ServicesProcessOrderStatusTaskUpdate();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ServicesProcessOrderStatusTaskUpdate()
        {
            IEnumerable<ADWebServicesProcessOrderStatusEntity> OrderToUpdateStatus = OrderStatusRepository.GetOrdersToUpdateStatus();
            foreach (ADWebServicesProcessOrderStatusEntity order in OrderToUpdateStatus)
            {
                try
                {
                    OrderStatusRepository.SetStatusAsSentToOrder(order);
                    try { OrderStatusRepository.AddLog(order, $"The order with Alfa code {order.IdADOrder} has been updated correctly.", ADTool.Log.LogType.Success); }
                    catch (Exception ex)
                    {
                        //TODO: Añadir log en fichero
                    }
                }
                catch (Exception ex)
                {
                    ex = ex.InnerException ?? ex;
                    ADWebWindowsServicesException ADException =
                            new ADWebWindowsServicesException(
                                ex.TargetSite.ToString(),
                                ex.Message,
                                ex.Source,
                                ex.TargetSite.DeclaringType.ToString(),
                                ex.TargetSite.MemberType.ToString(),
                                ex.StackTrace
                            );

                    try
                    {
                        OrderStatusRepository
                            .AddLog(
                                order,
                                $"It has ocurred an error while the order with Alfa's code {order.IdADOrder} was updating status.",
                                ADTool.Log.LogType.Error,
                                ADTool.Serializer.JsonSerializer.Serialize(ADException)
                        );
                    }
                    catch (Exception excep)
                    {
                        //TODO: Añadir log en fichero 
                    }
                }
            }
        }
    }
}
